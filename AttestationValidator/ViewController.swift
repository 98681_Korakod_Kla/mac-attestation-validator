//
//  ViewController.swift
//  AttestationValidator
//
//  Created by Korakod Saraboon on 15/12/2566 BE.
//

import Cocoa
import AppAttest

class ViewController: NSViewController {
  
  // attest
  @IBOutlet weak var keyIdTextField: NSTextField!
  @IBOutlet weak var bundleIdTextfield: NSTextField!
  @IBOutlet weak var teamIdTextField: NSTextField!
  @IBOutlet var attestTextView: NSTextView!
  @IBOutlet weak var challengeTextField: NSTextField!
  
  @IBOutlet weak var CertificatesCell: NSTextFieldCell!
  @IBOutlet weak var clientDataHasCell: NSTextFieldCell!
  @IBOutlet weak var createNouceCell: NSTextFieldCell!
  
  @IBOutlet weak var checknouceCell: NSTextFieldCell!
  @IBOutlet weak var checkKeyCell: NSTextFieldCell!
  @IBOutlet weak var appIDCell: NSTextFieldCell!
  
  @IBOutlet weak var checkCounterCell: NSTextFieldCell!
  @IBOutlet weak var aaguidCell: NSTextFieldCell!
  @IBOutlet weak var credentailId: NSTextFieldCell!
  
  @IBOutlet weak var keyResultLabel: NSTextField!
  
  // Assert
  @IBOutlet weak var asKeyIdTextField: NSTextField!
  @IBOutlet weak var asClientDataHashTextField: NSTextField!
  @IBOutlet weak var asChallengeTextField: NSTextField!
  @IBOutlet var asAssertDataTextView: NSTextView!
  
  @IBOutlet weak var asClientDataHash: NSTextFieldCell!
  @IBOutlet weak var asCheckNounce: NSTextFieldCell!
  @IBOutlet weak var asCreateSignature: NSTextFieldCell!
  @IBOutlet weak var asCheckAppIDHash: NSTextFieldCell!
  @IBOutlet weak var asCheckCounter: NSTextFieldCell!
  @IBOutlet weak var asCheckClientData: NSTextFieldCell!
  
  @IBOutlet weak var counterLabel: NSTextField!
  
  // viewModel
  var viewModel = AttestationViewModel()
  var isAlreadyAttest = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    viewModel.delegate = self
    bundleIdTextfield.stringValue = "com.scb.fasteasy.iphone"
    teamIdTextField.stringValue = "WF47643S5X"
  }
  
  override var representedObject: Any? {
    didSet {
      // Update the view, if already loaded.
    }
  }
  
  @IBAction func validateAttestButtonAction(_ sender: Any) {
    setAllDefault()
    startAttest()
  }
  
  @IBAction func validateAssertButtonAction(_ sender: Any) {
    if isAlreadyAttest {
      setASAllDefault()
      startAssert()
    }
  }
  
  
  func startAttest() {
    print("Start Testing Attest")
    // process in ios side
    let rawAttestation = attestTextView.string
    let rawKeyID = keyIdTextField.stringValue
    let rawChallenge = challengeTextField.stringValue
    
    // get from organization
    let teamID = teamIdTextField.stringValue
    let bundleID = bundleIdTextfield.stringValue
    
    // do attest
    viewModel.startAttest(keyID: rawKeyID, attestToken: rawAttestation, challenge: rawChallenge, teamID: teamID, bundleID: bundleID)
  }
  
  func startAssert() {
    print("Start Testing Assert")
    // process in ios side
    let rawKeyID = asKeyIdTextField.stringValue
    let rawAssertToken = asAssertDataTextView.string
    let rawChallenge = asChallengeTextField.stringValue
    let rawClientDatahash = asClientDataHashTextField.stringValue
    
    // do assert
    viewModel.startAssert(keyID: rawKeyID, assertToken: rawAssertToken, challenge: rawChallenge, clientDataHash: rawClientDatahash)
  }
  
  private func setAllResult(isSucess: Bool) {
    
    // set default result
    credentailId.backgroundColor =  isSucess ? .systemGreen : .systemRed
    checkCounterCell.backgroundColor = isSucess ? .systemGreen : .systemRed
    checkKeyCell.backgroundColor = isSucess ? .systemGreen : .systemRed
    appIDCell.backgroundColor = isSucess ? .systemGreen : .systemRed
    createNouceCell.backgroundColor = isSucess ? .systemGreen : .systemRed
    checknouceCell.backgroundColor = isSucess ? .systemGreen : .systemRed
    aaguidCell.backgroundColor = isSucess ? .systemGreen : .systemRed
    CertificatesCell.backgroundColor = isSucess ? .systemGreen : .systemRed
    clientDataHasCell.backgroundColor = isSucess ? .systemGreen : .systemRed
  }
  
  private func setASAllResult(isSucess: Bool) {
    asClientDataHash.backgroundColor = isSucess ? .systemGreen : .systemRed
    asCheckNounce.backgroundColor = isSucess ? .systemGreen : .systemRed
    asCreateSignature.backgroundColor = isSucess ? .systemGreen : .systemRed
    asCheckAppIDHash.backgroundColor = isSucess ? .systemGreen : .systemRed
    asCheckCounter.backgroundColor = isSucess ? .systemGreen : .systemRed
    asCheckClientData.backgroundColor = isSucess ? .systemGreen : .systemRed
  }
  
  private func setAllDefault() {
    // set default result
    keyResultLabel.stringValue = "Public key (sha256) :"
    credentailId.backgroundColor = .systemFill
    checkCounterCell.backgroundColor = .systemFill
    checkKeyCell.backgroundColor = .systemFill
    appIDCell.backgroundColor = .systemFill
    createNouceCell.backgroundColor = .systemFill
    checknouceCell.backgroundColor = .systemFill
    aaguidCell.backgroundColor = .systemFill
    CertificatesCell.backgroundColor = .systemFill
    clientDataHasCell.backgroundColor = .systemFill
  }
  
  private func setASAllDefault() {
    counterLabel.stringValue = "Counter :"
    asClientDataHash.backgroundColor = .systemFill
    asCheckNounce.backgroundColor = .systemFill
    asCreateSignature.backgroundColor = .systemFill
    asCheckAppIDHash.backgroundColor = .systemFill
    asCheckCounter.backgroundColor = .systemFill
    asCheckClientData.backgroundColor = .systemFill
  }
}

extension ViewController: AttestationViewModelDelegate {
  func updateSuccessAttest(result: AppAttest.AttestationResult) {
    keyResultLabel.stringValue = " \(keyResultLabel.stringValue ) \(result.publicKey)"
    setAllResult(isSucess: true)
    isAlreadyAttest = true
  }
  
  func updateSuccessAssert(result: AppAttest.AssertionResult) {
    counterLabel.stringValue = " \(counterLabel.stringValue ) \(result.counter)"
    setASAllResult(isSucess: true)
  }
  
  func updateFailedAttest(error: String) {
    isAlreadyAttest = false
    switch error {
    case "invalidNonce":
      checknouceCell.backgroundColor = .systemRed
    case "invalidAppIDHash":
      appIDCell.backgroundColor = .systemRed
    case "invalidPublicKey":
      checkKeyCell.backgroundColor = .systemRed
    case "invalidCounter":
      checkCounterCell.backgroundColor = .systemRed
    case "invalidCredentialID":
      credentailId.backgroundColor = .systemRed
    case "invalidClientHashData(Mod)":
      clientDataHasCell.backgroundColor = .systemRed
    default:
      setAllResult(isSucess: false)
    }
  }
  
  func updateFailedAssert(error: String) {
    switch error {
    case "invalidAppID":
      asCheckAppIDHash.backgroundColor = .systemRed
    case "invalidSignature":
      asCreateSignature.backgroundColor = .systemRed
    case "invalidCounter":
      asCheckCounter.backgroundColor = .systemRed
    case "invalidClientData":
      asCheckClientData.backgroundColor = .systemRed
    case "InvalidClientDataHash":
      asClientDataHash.backgroundColor = .systemRed
    case "InvalidNonce":
      asCheckNounce.backgroundColor = .systemRed
    default:
      setASAllResult(isSucess: false)
    }
  }
}

