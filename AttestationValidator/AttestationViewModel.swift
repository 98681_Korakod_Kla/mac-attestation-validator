//
//  AttestationViewModel.swift
//  AttestationValidator
//
//  Created by Korakod Saraboon on 16/12/2566 BE.
//

import Foundation
import AppAttest

protocol AttestationViewModelDelegate {
  func updateSuccessAttest(result: AppAttest.AttestationResult)
  func updateFailedAttest(error: String)
  func updateSuccessAssert(result: AppAttest.AssertionResult)
  func updateFailedAssert(error: String)
}

class AttestationViewModel {
  var delegate: AttestationViewModelDelegate?
  
  // mock store data
  private var appID: AppAttest.AppID?
  private var storeAttest = Dictionary<String, AppAttest.AttestationResult>()
  private var storedCounter: Int  = 0
  private var storePreviousAssert = Dictionary<String,  AppAttest.AssertionResult>()
  
  func startAttest(keyID: String, attestToken: String, challenge: String, teamID: String, bundleID: String ) {
    // Retrieve these values from the HTTP request
    // that your app sends to the server
    guard let attestation: Data = Data(base64Encoded: attestToken) else {
      delegate?.updateFailedAttest(error: "invalidClientHashData(Mod)")
      return
    }
    guard let keyIDData: Data = Data(base64Encoded: keyID) else {
      delegate?.updateFailedAttest(error: "invalidPublicKey")
      return
    }
    
    guard let challenge: Data = challenge.data(using: .utf8) else {
      delegate?.updateFailedAttest(error: "invalidClientHashData(Mod)")
      return
    }
    
    // Construct the attestation request and app ID,
    // which are simple structs
    let request = AppAttest.AttestationRequest(attestation: attestation, keyID: keyIDData)
    appID = AppAttest.AppID(teamID: teamID, bundleID: bundleID)
    
    // Verify the attestation
    do {
      let result = try AppAttest.verifyAttestation(challenge: challenge, request: request, appID: appID!)
      // store data
      storeAttest[keyID] = result
      delegate?.updateSuccessAttest(result: result)
    } catch let error {
      let errorString: String = String(describing: error)
      delegate?.updateFailedAttest(error: errorString)
    }
  }
  
  func startAssert(keyID: String, assertToken: String, challenge: String, clientDataHash: String) {
    // Retrieve these values from the HTTP request
    // that your app sends to the server
    guard let assertion = Data(base64Encoded: assertToken) else {
      delegate?.updateFailedAssert(error: "InvalidClientData")
      return
    }
    
    // Retrieve the challenge you generated in the previous step
    guard let clientData: Data = Data(base64Encoded: clientDataHash) else {
      delegate?.updateFailedAssert(error: "InvalidClientDataHash")
      return
    }
    
    guard let challengeData: Data = challenge.data(using: .utf8) else {
      delegate?.updateFailedAssert(error: "InvalidNonce")
      return
    }
    
    guard let appidAttest = appID else {
      delegate?.updateFailedAssert(error: "invalidClientData")
      return
    }
    
    // Retrieve the attestation you stored previously
    let attestation = storeAttest[keyID]
    
    guard let publicKey = attestation?.publicKey else {
      delegate?.updateFailedAssert(error: "invalidSignature")
      return
    }
    
    // If this is not the first assertion for this instance
    // of the app (i.e. for this unique key ID),
    // retrieve the previous AssertionResult. Otherwise,
    // use nil for this value.
    let previousAssertion = storePreviousAssert[keyID]
    
    // Construct the assertion request
    let requestBody = AppAttest.AssertionRequest(assertion: assertion, clientData: clientData, challenge: challengeData)
    
    do {
      let result = try AppAttest.verifyAssertion(challenge: challengeData,
                                                 request: requestBody,
                                                 previousResult: previousAssertion,
                                                 publicKey: publicKey,
                                                 appID: appidAttest )
      // store if success
      storePreviousAssert[keyID] = result as AppAttest.AssertionResult
      delegate?.updateSuccessAssert(result: result)
    } catch let error {
      let errorString: String = String(describing: error)
      delegate?.updateFailedAssert(error: errorString)
    }
  }
}
